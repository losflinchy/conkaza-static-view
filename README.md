# Conkaza Static Views #

### What is this repository for? ###

* Creation of static view 
* Version 1.0.0

### How do I get set up? ###

* Requirement
- Node 

* Dependencies
- Live-server
- Node-sass
- Sass-lint
- Concurrently

* Installation
- npm install
- npm start


### Who do I talk to? ###

* Eulier González
* Omar López